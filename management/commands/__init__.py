# -*- coding:utf-8 -*-
import datetime
import os
import re
import urllib2
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.4 fallback.

from django.utils.decorators import available_attrs, method_decorator
from django.contrib.auth.models import User
from django.core.files.base import ContentFile

from ... import models, radio_client

def lock(file_name):
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def _wrapped_view(*args, **kwargs):
            try:
                lockfile = os.open(file_name, os.O_CREAT | os.O_EXCL)
            except OSError:
                return
            try:
                result = func(*args, **kwargs)
            finally:
                os.close(lockfile)
                os.unlink(file_name)
            return result
        return _wrapped_view
    return decorator


class RadioSynchronizer(object):
    LOCKFILE = "/tmp/radio-sync.lock"
    FILENAME_RE = re.compile('.*/([^/]+)')
    EXTENSION_RE = re.compile('.*(\.[^.]+$)')

    def __init__(self, radio_instance):
        self.client = radio_client.Client(domain='test')
        self.radio_instance = radio_instance
        self.radio = radio_client.Radio(self.client,
                                        radio_client.get_radio_resource_uri(radio_instance.token))

    def _fetch_image(self, instance, image_url, attr='image'):
        try:
            image_content = urllib2.urlopen(image_url.encode('utf-8')).read()
        except urllib2.URLError:
            return
        except AttributeError:
            import urllib
            image_content = urllib.urlopen(image_url.encode('utf-8')).read()
        match = RadioSynchronizer.FILENAME_RE.match(image_url)
        if match:
            file_name = match.group(1)
        else:
            #FIXME: parse as in satchless image
            extension_match = RadioSynchronizer.EXTENSION_RE.match(image_url)
            file_name = 'default'
            if extension_match:
                file_name += extension_match.group(1)
        getattr(instance, attr).save(file_name, ContentFile(image_content))
        setattr(instance, '%s_url' % attr, image_url)

    def _fetch_episode_tracks(self, mirror_episode, api_episode):
        mirror_episode.tracks.all().delete()
        for api_track in api_episode.tracks:
            initial = api_track.get_json()
            initial['start'] = initial['start'].seconds
            initial['episode'] = mirror_episode
            mirror_episode.tracks.create(**initial)

    def _create_or_update_member(self, api_member):
        user, created = User.objects.get_or_create(username=api_member.username, defaults={
            'email': api_member.email
        })
        if not created and api_member.email != user.email:
            user.email = api_member.email
            user.save()
        initials = {
            'radio': self.radio_instance,
            'summary': api_member.summary,
            'websites': api_member.websites,
            'is_active': api_member.is_active,
            'user': user,
        }
        member_instance, c = models.RadioMember.objects.get_or_create(username=api_member.username,
                                                             defaults=initials)
        if not created:
            for attr, value in initials.items():
                setattr(member_instance, attr, value)
            if member_instance.avatar_url:
                self._fetch_image(member_instance, api_member.avatar_url,
                                  attr='avatar')
        if member_instance.avatar_url != api_member:
            if api_member.avatar_url:
                self._fetch_image(member_instance, api_member.avatar_url, attr='avatar')
            else:
                member_instance.avatar = ''
            member_instance.save()

    def _update_members(self):
        for member in self.radio.members:
            self._create_or_update_member(member)

    def _create_or_update_programme(self, programme, with_episodes=False):
        initial = {
            'radio': self.radio_instance,
            'title': programme.name,
            'description': programme.description,
            'color': programme.color,
        }
        if not models.Programme.objects.filter(resource_uri=programme.resource_uri).exists():
            programme_instance, created = models.Programme.objects.get_or_create(
                resource_uri=programme.resource_uri, **initial)
        else:
            programme_instance = models.Programme.objects.get(resource_uri=programme.resource_uri)
            created = False

        programme_instance.authors.clear()
        for author in programme.authors:
            author_instance = self.username2member(author.username)
            programme_instance.authors.add(author_instance)

        if not created:
            if programme_instance.title != programme.name \
                    or programme_instance.image_url != programme.image_url \
                    or programme_instance.description != programme.description \
                    or programme_instance.color != programme.color:

                if not programme.image_url:
                    programme_instance.image_url = ''
                    programme_instance.image = ''
                elif programme_instance.image_url != programme.image_url:
                    self._fetch_image(programme_instance, programme.image_url)
                programme_instance.title = programme.name
                programme_instance.description = programme.description
                programme_instance.color = programme.color
                programme_instance.image_url = programme.image_url
                programme_instance.save()
        else:
            if not programme.image_url:
                programme_instance.image_url = ''
                programme_instance.image = ''
            else:
                self._fetch_image(programme_instance, programme.image_url)
            programme_instance.save()
        if with_episodes:
            for episode in programme.programme_episode.fetch():
                self._create_or_update_programme_episode(episode, programme_instance)
        return programme_instance

    def _create_or_update_programme_episode(self, programme_episode, programme_instance=None):
        if not programme_instance:
            try:
                programme_instance = models.Programme.objects.get(
                    resource_uri=programme_episode.programme_uri)
            except models.Programme.DoesNotExists:
                programme_instance = self._create_or_update_programme(programme_episode.programme)

        author_instance = self.username2member(programme_episode.author.username)
        initials = {
            'author': author_instance,
            'name': programme_episode.name,
            'description': programme_episode.description,
            'image_url': programme_episode.image_url,
            'programme': programme_instance,
            'podcast_url': programme_episode.podcast_url,
            'podcast_length': programme_episode.podcast_length,
        }

        episode_instance, created = models.ProgrammeEpisode.with_deleted.get_or_create(
            resource_uri=programme_episode.resource_uri, defaults=initials)

        episode_instance.authors.clear()
        for author in programme_episode.authors:
            author_instance = self.username2member(author.username)
            episode_instance.authors.add(author_instance)

        if not created:
            if episode_instance.image_url != programme_episode.image_url:
                self._fetch_image(episode_instance, programme_episode.image_url)
            for attr, value in initials.items():
                setattr(episode_instance, attr, value)
            episode_instance.save()
        elif programme_episode.image_url:
            self._fetch_image(episode_instance, episode_instance.image_url)
        self._fetch_episode_tracks(episode_instance, programme_episode)
        return episode_instance

    def username2member(self, username):
        if not hasattr(self, '_username2member'):
            self._username2member = dict((m.username, m) for m in models.RadioMember.objects.all())
        if username not in self._username2member:
            self._update_members()
            self._username2member.clear()
            for member in models.RadioMember.objects.all():
                self._username2member[member.username] = member
        return self._username2member[username]

    def sync_stream(self, stream, start):
        stream_instance, created = self.radio_instance.streams.get_or_create(resource_uri=stream.resource_uri)
        start = start or datetime.date.today()
        end = start + datetime.timedelta(days=7)
        programmes = dict((p.resource_uri, p) for p in models.Programme.objects.all())

        transmissions_for_removal = dict((t.resource_uri,t)
                                         for t in stream_instance.transmissions.filter(start__gt=start,
                                                                                       end__lt=end))
        for transmission in stream.transmission.fetch(date_from=start, date_to=end):
            if not transmission.programme_uri:
                continue
            if transmission.programme_uri not in programmes:
                programme_instance = self._create_or_update_programme(transmission.programme)
                programmes[programme_instance.resource_uri] = programme_instance
            else:
                programme_instance = programmes[transmission.programme_uri]

            if not transmission.programme_episode_uri:
                episode_instance = None
            elif not models.ProgrammeEpisode.objects.filter(
                    resource_uri=transmission.programme_episode_uri).exists():
                episode_instance = self._create_or_update_programme_episode(transmission.programme_episode,
                                                                            programme_instance)
            else:
                episode_instance = self._create_or_update_programme_episode(transmission.programme_episode)

            initials = {
                'start': transmission.start,
                'end': transmission.end,
                'episode': episode_instance,
                'programme': programme_instance,
            }

            transmission_instance, created = stream_instance.transmissions.get_or_create(
                resource_uri=transmission.resource_uri, defaults=initials)
            if not created:
                for attr, value in initials.items():
                    setattr(transmission_instance, attr, value)
                transmission_instance.save()
            if transmission.resource_uri in transmissions_for_removal:
                del transmissions_for_removal[transmission.resource_uri]
        for transmission_instance in transmissions_for_removal.values():
            transmission_instance.delete()

    @method_decorator(lock(LOCKFILE))
    def run(self, start=None):
        if self.radio_instance.updated is None:
            for programme in self.radio.programme.fetch():
                self._create_or_update_programme(programme, with_episodes=True)
        else:
            programmes = self.radio.programme_updates.fetch(
                updated_since=self.radio_instance.updated)
            for programme in programmes:
                self._create_or_update_programme(programme)
            episodes = self.radio.programme_episode_updates.fetch(
                updated_since=self.radio_instance.updated)
            for episode in episodes:
                self._create_or_update_programme_episode(episode)

        for stream in self.radio.stream.fetch():
            self.sync_stream(stream, start=start)

        self.radio_instance.updated = datetime.datetime.now()
        self.radio_instance.save()

    #@method_decorator(lock(LOCKFILE))
    #def sync_programmes(self):
    #    if self.radio.updated:
    #        for programme in self.radio.programme.fetch():
    #            self._create_or_update_programme(programme, with_episodes=True)
    #    else:
    #        updated = self.radio.programme_updates.updated.fetch(updated_since=self.radio.updated)

