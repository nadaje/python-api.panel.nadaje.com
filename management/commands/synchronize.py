from django.core.management.base import NoArgsCommand

from . import RadioSynchronizer
from ... import models


class Command(NoArgsCommand):
    LOCKFILE = "/tmp/radio-sync.lock"

    def handle(self, *args, **kwargs):
        for radio in models.Radio.objects.all():
            synchronizer = RadioSynchronizer(radio_instance=radio)
            synchronizer.run()



