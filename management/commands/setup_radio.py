from django.core.management.base import NoArgsCommand

from . import RadioSynchronizer
from ... import models

class Command(NoArgsCommand):
    LOCKFILE = "/tmp/programme-sync.lock"

    def handle(self, *args, **kwargs):
#        synchronizer = RadioSynchronizer(domain=settings.RADIO_API_DOMAIN,
#                                         radio_uri=settings.RADIO_API_PATH)
#        synchronizer.sync_programmes()
        for radio in models.Radio.objects.all():
            synchronizer = RadioSynchronizer(radio_instance=radio)
            synchronizer.sync_programmes()
