# -*- coding:utf-8 -*-
try:
    import unidecode
except:
    unidecode = None

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from .fields import TimedeltaField

# This is example aplication - you can use it if you want
# to mirror radio resources.

class Radio(models.Model):
    token = models.CharField(max_length=40, unique=True)
    updated = models.DateTimeField(null=True, editable=True)


class Stream(models.Model):
    radio = models.ForeignKey(Radio, related_name='streams')
    resource_uri = models.CharField(max_length=512, unique=True)
    name = models.CharField(max_length=512)
    updated = models.DateTimeField(null=True, editable=True)


class RadioMember(models.Model):
    radio = models.ForeignKey(Radio)
    username = models.CharField(max_length=512, unique=True)
    avatar = models.ImageField(upload_to=getattr(settings, 'AVATAR_UPLOAD_DIR','upload/images/avatars'))
    avatar_url = models.CharField(max_length=512, blank=True)
    summary = models.TextField(blank=True)
    websites = models.TextField(blank=True)
    user = models.OneToOneField(User, null=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.username


#def attach_radio_member(sender, instance, created, **kwargs):
#    if (created and not kwargs.get('raw', False)):
#        radio_member, created = RadioMember.objects.get_or_create(username=instance.username)
#        radio_member.user = instance
#        radio_member.save()
#models.signals.post_save.connect(attach_radio_member, sender=User)


def generate_slug(instance, prepopulate_from, slug_attr, queryset=None):
    value = getattr(instance, prepopulate_from)
    if unidecode:
        value = unidecode.unidecode(value)
    suffix = 0
    potential = base = slugify(value)
    if not potential:
        potential = base = '_'
    queryset = queryset if queryset is not None else instance.__class__.objects.all()
    while True:
        if suffix:
            potential = "-".join([base, str(suffix)])
        if not queryset.filter(**{slug_attr: potential}).exclude(pk=instance.pk).count():
            value = potential
            break;
        suffix += 1
    return value


class Programme(models.Model):
    radio = models.ForeignKey(Radio)
    resource_uri = models.CharField(max_length=512, primary_key=True)

    title = models.CharField(max_length=512)
    description = models.TextField(blank=True)
    authors = models.ManyToManyField(RadioMember, related_name="programmes")
    slug = models.SlugField(max_length=512, unique=True)

    image_url = models.CharField(max_length=512, blank=True)
    image = models.ImageField(upload_to=getattr(settings, 'PROGRAMME_IMAGE_UPLOAD_DIR','upload/images/programmes'))

    color = models.CharField(max_length=6, blank=True)

    channel = models.BooleanField(default=False)

    class Meta:
        ordering = ('title',)

    def save(self, *args, **kwargs):
        slug = generate_slug(self, 'title', 'slug')
        if slug != self.slug:
            self.slug = slug
        super(Programme, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title


class ProgrammeEpisodeManager(models.Manager):
    def get_query_set(self, *args, **kwargs):
        return super(ProgrammeEpisodeManager, self).get_query_set(*args, **kwargs).filter(deleted=False)


class ProgrammeEpisode(models.Model):
    objects = ProgrammeEpisodeManager()
    with_deleted = models.Manager()

    resource_uri = models.CharField(max_length=512, unique=True)

    name = models.CharField(max_length=512)
    slug = models.SlugField(max_length=512, unique=True)
    description = models.TextField()
    programme = models.ForeignKey(Programme, null=True, related_name="episodes")
    author = models.ForeignKey(RadioMember, related_name="episodes")
    authors = models.ManyToManyField(RadioMember, related_name="episodes_coauthor",
                                     null=True)

    podcast_url = models.URLField(blank=True)
    podcast_length = models.IntegerField(blank=True, default=0)
    image_url = models.URLField(blank=True)
    image = models.ImageField(upload_to=getattr(settings, 'PROGRAMME_IMAGE_UPLOAD_DIR','upload/images/programmes'), blank=True)
    generated = models.BooleanField(default=False)

    deleted = models.BooleanField(default=False)
    premiere = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        slug = generate_slug(self, 'name', 'slug', queryset=self.__class__.with_deleted.all())
        if slug != self.slug:
            self.slug = slug
        super(ProgrammeEpisode, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name


class EpisodeTrack(models.Model):
    episode = models.ForeignKey(ProgrammeEpisode, related_name='tracks')
    title = models.CharField(max_length=512, blank=True)
    artist = models.CharField(max_length=512, blank=True)
    start = TimedeltaField(blank=False, default=0)

    class Meta:
        ordering = ('start',)

    def __unicode__(self):
        if not self.artist and not self.title:
            return "[unknown artist] - “[unknown title]”"
        if not self.artist or not self.title:
            return self.artist or self.title
        return u"%s - “%s”" % (self.artist, self.title)


class MusicSet(models.Model):
    stream = models.ForeignKey(Stream, related_name='transmissions')
    resource_uri = models.CharField(max_length=512, db_index=True, null=True)
    start = models.DateTimeField()
    end = models.DateTimeField()
    episode = models.ForeignKey(ProgrammeEpisode, related_name="music_sets",
                                null=True)
    programme = models.ForeignKey(Programme)

    class Meta:
        ordering = ('start',)


def delete_untrasmitted_episode(sender, instance, **kwargs):
    if ProgrammeEpisode.objects.filter(pk=instance.episode_id).exists() and not instance.episode.music_sets.exists():
        instance.episode.deleted = True
        instance.episode.save()
models.signals.post_delete.connect(delete_untrasmitted_episode, MusicSet)

def update_premiere(sender, instance, **kwargs):
    if not kwargs.get('raw', False) and instance.episode_id:
        premiere = instance.episode.music_sets.aggregate(premiere=models.Min('start'))['premiere']
        if premiere != instance.episode.premiere:
            instance.episode.premiere = premiere
            instance.episode.save()
models.signals.post_save.connect(update_premiere, MusicSet)
models.signals.post_delete.connect(update_premiere, MusicSet)
