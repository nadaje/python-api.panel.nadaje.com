# -coding: utf-8 -
"""Loosely inspired by mirumee/python-invoicible API library"""

import datetime
import httplib
import simplejson
import iso8601
import urllib

DELETED = 204
BAD_REQUEST = 400
FORBIDDEN = 401
NOT_FOUND = 404
DUPLICATE_ENTRY = 409
NOT_HERE = 410
INTERNAL_ERROR = 500
NOT_IMPLEMENTED = 501
THROTTLED = 503

DEBUG=True

DATE_FORMAT = '%Y-%m-%d'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

PAGE_MAX_SIZE = 80


class ApiError(Exception):

    pass


class DoesNotExists(Exception):

    pass


class ValidationError(Exception):

    pass


def _format_date(date):
    return date if isinstance(date, basestring) else date.strftime(DATE_FORMAT)

def _format_datetime(dt):
    return dt if isinstance(dt, basestring) else dt.strftime(DATETIME_FORMAT)


class Client(object):

    def __init__(self, domain='api.panel.nadaje.com'):
        self.domain = domain

        self.connection = httplib.HTTPConnection(self.domain)
        self.protocol = 'http'
        self.json_encoder = simplejson.JSONEncoder()

    def get_resources(self, path, query=None):
        query = query or {}
        query['format'] = 'json'
        self.connection = httplib.HTTPConnection(self.domain)
        self.connection.request('GET', path + '?' + urllib.urlencode(query))
        response = self.connection.getresponse()
        json = response.read()
        if response.status != 200:
            if response.status == 404:
                raise DoesNotExists()
            elif response.status == 400:
                raise ValidationError(response.body)
            else:
                raise ApiError('Unknown response status: %s, %s' % (response.status,
                                                                    response.read()))

        return simplejson.loads(json)

class ApiFieldDescriptor(object):
    def __init__(self, prepopulate_from, klass):
        self.prepopulate_from = prepopulate_from
        self.klass = klass
        self.name = '_' + prepopulate_from[:-4]

    def __get__(self, api_object, api_class):
        if getattr(api_object, self.prepopulate_from, None):
            if not api_object.__dict__.get(self.name, None):
                resource_uri = getattr(api_object, self.prepopulate_from)
                api_object.__dict__[self.name] = self.klass(api_object._client, resource_uri)
        else:
            api_object.__dict__[self.name] = None

        return api_object.__dict__[self.name]

    def __set__(self, api_object, value):
        if not hasattr(api_object, 'resource_uri') or not getattr(api_object, 'resource_uri', None) \
          or not isinstance(value, self.klass):
            raise Exception("You can assign only saved %s instances." % self.klass.__name__)
        setattr(api_object, self.prepopulate_from, value.resource_uri)
        api_object.__dict__[self.name] = value

class ApiManagerFieldDescriptor(object):
    def __init__(self, prepopulate_from, manager_klass):
        self.prepopulate_from = prepopulate_from
        self.manager_klass = manager_klass
        self.name = '_' + prepopulate_from[:-4]

    def __get__(self, api_object, api_class):
        if not api_object.__dict__.get(self.name, None):
            resource_uri = getattr(api_object, self.prepopulate_from)
            manager = self.manager_klass(client=api_object._client, resource_uri=resource_uri)
            api_object.__dict__[self.name] = manager
        return api_object.__dict__[self.name]

    def __set__(self, api_object, value):
        raise Exception("""You can't assign to this field: You can only iterate through it's content by all() method
and create items with create() method""")

class ApiObject(object):
    _resource_uri = None
    _fields = {}

    def __init__(self, client, resource_uri=None, json=None):
        self._client = client
        if not resource_uri and not json:
            raise ValueError('You have to initialize object with resource_uri'
                             'or json data')
        if resource_uri:
            self.resource_uri = resource_uri
            json = self._client.get_resources(self.resource_uri)
        else:
            self.resource_uri = None

        if json:
            self.parse_json(json)

    def get_json(self):
        data = {}
        for f, t in self._fields.items():
            # getting field value
            try:
                field = getattr(self, f)
            except AttributeError:
                # maybe if it's required field it should raise an exception?
                break
            if t is datetime.datetime:
                data[f] = field.strftime(DATETIME_FORMAT)
            elif t is datetime.date:
                data[f] = field.strftime(DATE_FORMAT)
            elif issubclass(t, list):
                data[f] = map(lambda item: item.get_json(), field)
            elif not isinstance(field, ApiObject) \
              and (not isinstance(field, type) or not issubclass(field, ApiObject)):
                data[f] = field
        return data

    def parse_json(self, data, raw_json = False):
        if raw_json:
            data = simplejson.loads(data)
        data = self._parse_json(data)
        for f in data.keys():
            setattr(self, f, data[f])

    def _parse_json(self, data):
        d = {}
        if not isinstance(data, dict):
            raise ValidationError(u'Incorrect data type, expected dict, received: %s.' % type(data))
        for f, t in self._fields.items():
            try:
                value = data[f]
            except KeyError:
                if DEBUG:
                    print 'missing key', f
                continue
            try:
                if t is datetime.datetime:
                    value = iso8601.parse_date(value)
                elif t is datetime.date:
                    value = iso8601.parse_json(value)
                elif issubclass(t, list) and hasattr(t, 'item_klass'):
                    value = map(lambda item: t.item_klass(**item), value)
                elif issubclass(t, ApiListItem):
                    value = t(**value)
                elif value is not None:
                    value = t(value)
                else:
                    value = None
            except Exception, e:
                raise ValidationError('Incorrect type for %s: %s=%s? (%s)' % ( self.__class__.__name__, f, value, e))
            d[f] = value
        return d

    def __str__(self):
        return str(self.get_json())

    def __repr__(self):
        return str(self.get_json())


class ApiObjectResultSet(object):

    def __init__(self, client, resource_uri, api_klass, query, limit=PAGE_MAX_SIZE,
                 offset=0):
        self._client = client
        self._api_klass = api_klass
        self._resource_uri = resource_uri

        self.query = query or {}
        self.limit = limit
        self.offset = offset

        self.last_slice = False
        self.current_slice = None

    def _fetch_next_slice(self):
        if self.last_slice:
            return None
        query = dict(self.query, offset=self.offset, limit=self.limit)
        resources = self._client.get_resources(self._resource_uri,
                                               query=query)
        self.offset += len(resources)
        if len(resources) < self.limit:
            self.last_slice = True
        return resources

    def next(self):
        if not self.current_slice:
            self.current_slice = self._fetch_next_slice()
            if not self.current_slice:
                raise StopIteration()
        return self._api_klass(client=self._client,
                               json=self.current_slice.pop())

    def __iter__(self):
        return self


class ApiObjectManager(object):
    api_klass = None
    _resource_uri = None

    def __init__(self, client, resource_uri=None):
        self._client = client
        self._resource_uri = resource_uri or self._resource_uri or self.api_klass._resource_uri

    def all(self, client=None):
        result = []
        resources = self._client.get_resources(self._resource_uri)
        for resource in resources:
            result.append(self.api_klass(self._client, json=resource))
        return result

    def fetch(self, offset=0, limit=40, query=None):
        result = []
        query = dict({'offset': offset, 'limit': limit}, **query) if query else {'offset': offset, 'limit': limit}
        resources = self._client.get_resources(self._resource_uri,
                                               query=query)
        for resource in resources:
            result.append(self.api_klass(self._client, json=resource))
        return result

    def create(self, **kwargs):
        resource = self._client.create_resource(self._resource_uri, kwargs)
        return self.api_klass(self._client, json=resource)


class ApiObjectUpdatesManager(object):
    api_klass = None
    _resource_uri = None

    def __init__(self, client, resource_uri):
        self._client = client
        self._resource_uri = resource_uri

    def fetch(self, updated_since):
        updated_since = _format_datetime(updated_since)
        query = dict(updated_since=updated_since)
        return ApiObjectResultSet(client=self._client,
                                  api_klass=self.api_klass,
                                  resource_uri=self._resource_uri,
                                  query=query)


class DateSliceableApiObjectManager(ApiObjectManager):
    def fetch(self, *args, **kwargs):
        date_from, date_to = kwargs.pop('date_from', None), kwargs.pop('date_to', None)
        query = kwargs.setdefault('query', {})
        if date_from:
            query['date_from'] = date_from if isinstance(date_from, basestring) else date_from.strftime(DATE_FORMAT)
        if date_to:
            query['date_to'] = date_to if isinstance(date_to, basestring) else date_to.strftime(DATE_FORMAT)
        return super(DateSliceableApiObjectManager, self).fetch(*args, **kwargs)


class ApiListItem(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if self._fields[key] is datetime.datetime:
                value = iso8601.parse_date(value)
            elif self._fields[key] is datetime.date:
                value = iso8601.parse_date(value)
            elif self._fields[key] is datetime.timedelta:
                value = datetime.timedelta(seconds=int(value))
            else:
                value = self._fields[key](value)
            setattr(self, key, value)

    def get_json(self):
        result = {}
        for key in self._fields.keys():
            result[key] = getattr(self, key, None)
        return result

    def __unicode__(self):
        return str(self.get_json())

    def __repr__(self):
        return str(self.get_json())


class Author(ApiListItem):

    _fields = {
        'username': unicode,
    }


class Authors(list):

    item_klass = Author


class MusicTrack(ApiListItem):
    _fields = {
        'start': datetime.timedelta,
        'title': unicode,
        'artist': unicode,
    }


class MusicTrackList(list):

    item_klass = MusicTrack


class ProgrammeEpisode(ApiObject):

    _fields = {
        'author': Author,
        'authors': Authors,
        'name': unicode,
        'description': unicode,
        'image_url': unicode,
        'podcast_url': unicode,
        'podcast_length': int,
        'programme_uri': unicode,
        'resource_uri': unicode,
        'tracks': MusicTrackList,
    }

    @property
    def programme(self):
        if not hasattr(self, '_programme'):
            self._programme = Programme(self._client, self.programme_uri)
        return self._programme


class ProgrammeEpisodeManager(ApiObjectManager):

    api_klass = ProgrammeEpisode


class ProgrammeEpisodeUpdatesManager(ApiObjectUpdatesManager):

    api_klass = ProgrammeEpisode


class Programme(ApiObject):

    _fields = {
        'resource_uri': unicode,
        'programme_episode_uri': unicode,
        'author': Author,
        'authors': Authors,
        'color': unicode,
        'description': unicode,
        'image_url': unicode,
        'name': unicode,
        'updated': datetime.datetime,
    }
    programme_episode = ApiManagerFieldDescriptor('programme_episode_uri',
                                                  ProgrammeEpisodeManager)


class ProgrammeManager(ApiObjectManager):

    api_klass = Programme


class ProgrammeUpdatesManager(ApiObjectUpdatesManager):

    api_klass = Programme


class Transmission(ApiObject):

    _fields = {
        'programme_uri': unicode,
        'programme_episode_uri': unicode,
        'resource_uri': unicode,
        'start': datetime.datetime,
        'end': datetime.datetime,
        'transmission_type': unicode,
    }
    programme = ApiFieldDescriptor('programme_uri', Programme)
    programme_episode = ApiFieldDescriptor('programme_episode_uri',
                                            ProgrammeEpisode)


class TransmissionManager(DateSliceableApiObjectManager):

    api_klass = Transmission


class Stream(ApiObject):

    _fields = {
        'resource_uri': unicode,
        'transmission_uri': unicode,
        'name': unicode,
    }
    transmission = ApiManagerFieldDescriptor('transmission_uri',
                                                       TransmissionManager)


class StreamManager(ApiObjectManager):

    api_klass = Stream


class RadioMember(ApiListItem):

    _fields = {
        'avatar_url': unicode,
        'is_active': bool,
        'summary': unicode,
        'username': unicode,
        'websites': unicode,
        'email': unicode,
    }


class RadioMembers(list):

    item_klass = RadioMember


class Radio(ApiObject):

    _resource_uri = None
    _fields = {
        'resource_uri': unicode,
        'stream_uri': unicode,
        'name': unicode,
        'programme_uri': unicode,
        'programme_updates_uri': unicode,
        'programme_episode_updates_uri': unicode,
        'members': RadioMembers,
    }
    stream = ApiManagerFieldDescriptor('stream_uri', StreamManager)
    programme = ApiManagerFieldDescriptor('programme_uri', ProgrammeManager)
    programme_updates = ApiManagerFieldDescriptor('programme_updates_uri',
                                                  ProgrammeUpdatesManager)
    programme_episode_updates = ApiManagerFieldDescriptor('programme_episode_updates_uri',
                                                          ProgrammeEpisodeUpdatesManager)


def get_radio_resource_uri(token):
    return '/api/2.0/radio/%s/' % token
