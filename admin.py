# -*- coding:utf-8 -*-
from django.contrib import admin
from . import models

class EpisodeTrackInline(admin.TabularInline):
    model = models.EpisodeTrack

class EpisodeTransmissionInline(admin.TabularInline):
    model = models.MusicSet

class ProgrammeEpisodeAdmin(admin.ModelAdmin):
    inlines = [EpisodeTransmissionInline, EpisodeTrackInline]

admin.site.register(models.ProgrammeEpisode, ProgrammeEpisodeAdmin)
admin.site.register(models.EpisodeTrack)
admin.site.register(models.Programme)
admin.site.register(models.RadioMember)
